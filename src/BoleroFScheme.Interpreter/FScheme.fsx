#r "FScheme.dll"

open BoleroFScheme.Interpreter.FScheme

let calc input =
    let output = input |> rep environment
    printf "\n%s\n" output
    output
