module BoleroFScheme.Client.Main

open Elmish
open Bolero
open Bolero.Html
open BoleroFScheme.Interpreter.FScheme

type Model =
    { input: string
      output: string }

let initModel =
    { input = ";;Enter Scheme code and hit Run to see the output!"
      output = "" }

type Message =
    | Run
    | SetInput of string

let scheme input = input |> rep environment

let update message model =
    match message with
    | Run -> { model with output = scheme model.input }
    | SetInput input -> { model with input = input }

let view model dispatch =
    div []
        [ textarea
            [ on.change (fun args -> dispatch (SetInput(unbox args.Value)))
              attr.rows 34
              attr.cols 80 ] [ text model.input ]
          button [ on.click (fun _ -> dispatch Run) ] [ text "Run" ]
          textarea
              [ attr.rows 34
                attr.cols 80 ] [ text model.output ] ]


type MyApp() =
    inherit ProgramComponent<Model, Message>()

    override this.Program = Program.mkSimple (fun _ -> initModel) update view
